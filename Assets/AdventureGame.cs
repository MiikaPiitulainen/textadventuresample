﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdventureGame : MonoBehaviour
{

   [SerializeField] Text textComponent;
   [SerializeField] State FisrtState;
   [SerializeField] State TiredState;


    State currentState;
    int tirednessLevel;
    bool isResetable;

    // Start is called before the first frame update
    void Start()
    {
        currentState = FisrtState;
        textComponent.text = currentState.GetStateStory();
        
    }


    // Update is called once per frame
    void Update()
    {
        ManageState();
    }

    private void ManageState()
    {
        var NextStates = currentState.GetNextStates();


        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentState = NextStates[0];
            tirednessLevel = tirednessLevel + currentState.GetTiredness();
            Debug.Log(tirednessLevel);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentState = NextStates[1];
            tirednessLevel = tirednessLevel + currentState.GetTiredness();
            Debug.Log(tirednessLevel);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentState = NextStates[2];
            tirednessLevel = tirednessLevel + currentState.GetTiredness();
            Debug.Log(tirednessLevel);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            currentState = NextStates[3];
            tirednessLevel = tirednessLevel + currentState.GetTiredness();
            Debug.Log(tirednessLevel);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            currentState = NextStates[4];
            tirednessLevel = tirednessLevel + currentState.GetTiredness();
            Debug.Log(tirednessLevel);
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }



        if (tirednessLevel>=10)
        {
            currentState = TiredState;
            textComponent.text = currentState.GetStateStory();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            isResetable = currentState.GetResetable();
            if (isResetable == true)
            {
                currentState = FisrtState;
                textComponent.text = currentState.GetStateStory();
                tirednessLevel = 0;
            }
        }
        textComponent.text = currentState.GetStateStory();
    }
}
