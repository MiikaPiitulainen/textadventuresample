﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="State")]

public class State: ScriptableObject
{

   [TextArea(14,10)] [SerializeField] string storyText;
   [SerializeField] State[] nextStates;
   [SerializeField] int tierdness;
   [SerializeField] bool resetable;
    public string GetStateStory()
    {
        return storyText;
    }
    public State[] GetNextStates()
    {
        return nextStates;
    }
    public int GetTiredness()
    {
        return tierdness;
    }
    public bool GetResetable()
    {
        return resetable;
    }

}
